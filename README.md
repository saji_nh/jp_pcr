## IMPORTANT 

The data format in the data provided by Kaz-Ogiwara may change without
notice. Be careful when processing archived data in this repository.
You will need to modify the 'sort_by_prefs.rb' file [i have archived
this file; so, you may find an appropriate one; however, pls. check
if the 'sort_by_prefs.rb' code you are using is compatible with the
data you are parsing]


## Processed data from Kaz-Ogiwara
(https://github.com/kaz-ogiwara/covid19/blob/master/README.md)

The data here are from kaz-ogiwara's github page, and use his data
described below. I have added some ruby scripts to pull the prefecture level
data into separate files (one per prefecture; eg. Tokyo.csv). There may be 
minor, but significant errors in the data such as reports of tests being
wrong, especially towards the beginning of each report.

The original data from kaz-ogiwara can found in the 'covid' subdirectory

# Coronavirus Disease (COVID-19) Situation Report in Japan
The state of the infection of the new coronavirus (COVID19) in Japan was summarized visually from a press release from the Ministry of Health, Labor and Welfare.

[JAPANESE](https://github.com/kaz-ogiwara/covid19/blob/master/README.md)

### Public page
- Toyo Keizai Online "Coronavirus Disease (COVID-19) Situation Report in Japan"
- https://toyokeizai.net/sp/visual/tko/covid19/en.html

### Data source
- Materials released by the Ministry of Health, Labor and Welfare
- https://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000121431_00086.html
