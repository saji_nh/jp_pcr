require 'fileutils'
require 'date'
require './idwr'
require 'csv'
require 'pp'
require 'matrix'

require './plot_helper.rb'

def process_cats(str)
  arr = CSV.parse(str)
  arr.first
end

reports = ["zensu"] #, "teiten", "teitenrui"]
report = "teitenrui"
num_headers = 3
num_prefs = 47+1 # 47 prefectures + all Japan
find_string = "2019-nCoV infection"
#find_string = "Tuberculosis"
#find_string = "Influenza(excld. avian influenza and pandemic influenza)"
find_string = /Influenza\(excld\./
write_dir = "Influenza"

data = []
pref_names = []
# Note -- there is a problem with data format for 2015; we need to
# resolve this before analysis
# data not complete for 2016

(2017..2020).to_a.each do |year|

  nweeks = num_weeks(year) #.to_i
  nweeks = 39 if year==2020
  # yr_data will hold data separately for all Japanese prefectures
  # se will hold data per sentinel separately for all Japanese prefectures
  yr_data = Matrix.build(nweeks,num_prefs) {|r,c| 0}
  se_data = Matrix.build(nweeks,num_prefs) {|r,c| 0.0}
  (1..nweeks).to_a.each do |iwk|

    fmt_wk = "%02d" % iwk

      rep_dir = File.join(report,year.to_s)
      Dir.chdir(rep_dir) do

        rep_path = "#{report}#{fmt_wk}.csv"
        fin = File.open(rep_path,"r")
        num_headers.times { fin.gets }
        cats = process_cats ( fin.gets.chomp )
        # we use the case equality operator below,
        # so that it can match against exact string or a regular expression
        idx = cats.index {|e| find_string === e}
        next unless idx # go to next week, if the disease is not yet 
                        # included in sentinel-watch till that week
        1.times {fin.gets}
        pref_names = []
        num_prefs.times do |iprf|
          fil_arr = process_cats(fin.gets.chomp)
          req_data = Integer(fil_arr[idx])
          pref_names << fil_arr[0]
          yr_data[iwk-1,iprf] = req_data
          se_data[iwk-1,iprf] = Float(fil_arr[idx+1])
        end # prefectures loop
       
        fin.close

      end # outside directory
  end # week
  nrow = yr_data.row_count
  ncol = yr_data.column_count
  dly_data = Matrix.build(nrow,ncol) {|r,c| 0}
  dly_sent = Matrix.build(nrow,ncol) {|r,c| 0.0}
  ncol.times do |icol|
    dly_data[0,icol] = yr_data[0,icol]
    dly_sent[0,icol] = se_data[0,icol]
  end
  (1..nrow-1).each do |irow|
    ncol.times do |icol|
      dly_data[irow,icol] = yr_data[irow,icol] - yr_data[irow-1,icol]
      dly_sent[irow,icol] = se_data[irow,icol] - se_data[irow-1,icol]
    end
  end
  FileUtils.mkdir_p File.join(write_dir,year.to_s)
  pref_names[0] = "Total"
  pref_names.each_with_index do |pref,ipref|
   p pref
    outfil = File.join(write_dir,year.to_s,"#{pref}.csv")
    CSV.open(outfil,"w") do |csv|
      nweeks.times do |iwk|
       csv << [(iwk+1),dly_data[iwk,ipref],dly_sent[iwk,ipref],yr_data[iwk,ipref],se_data[iwk,ipref]]
      end
    end
  end
  
end # year
exit
plot_graph(data)


#--- sources of data
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/zensu01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teiten01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teitenrui01.csv
