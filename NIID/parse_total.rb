require 'fileutils'
require 'date'
require './idwr'
require 'csv'
require 'pp'

require './plot_helper.rb'

reports = ["zensu"] #, "teiten", "teitenrui"]
report = "teitenrui"
num_headers = 3
num_prefs = 1 #only get the total for all of Japan
#find_string = "2019-nCoV infection"
#find_string = "Tuberculosis"
#find_string = "Influenza(excld. avian influenza and pandemic influenza)"
find_string = /Influenza\(excld\./

def process_cats(str)
  arr = CSV.parse(str)
  arr.first
end

data = []
pref_names = []
# Note -- there is a problem with data format for 2015; we need to
# resolve this before analysis
(2019..2020).to_a.each do |year|

  nweeks = num_weeks(year) #.to_i
  nweeks = 34 if year==2020
  yr_data = []
  (1..nweeks).to_a.each do |iwk|

    fmt_wk = "%02d" % iwk

      rep_dir = File.join(report,year.to_s)
      Dir.chdir(rep_dir) do

        rep_path = "#{report}#{fmt_wk}.csv"
        fin = File.open(rep_path,"r")
        num_headers.times { fin.gets }
        cats = process_cats ( fin.gets.chomp )
        # we use the case equality operator below,
        # so that it can match against exact string or a regular expression
        idx = cats.index {|e| find_string === e}
        next unless idx # go to next week, if the disease is not yet 
                        # included in sentinel-watch till that week
        1.times {fin.gets}
        pref_names = []
        num_prefs.times do |iprf|
          fil_arr = process_cats(fin.gets.chomp)
          req_data = Integer(fil_arr[idx])
          pref_names << fil_arr[0]
          yr_data << req_data
        end # prefectures loop
       
        fin.close

      end # outside directory
  end # week
  dly_data = []
  dly_data[0] =  yr_data[0]
  (1..yr_data.length-1).each do |i|
    dly_data[i] = yr_data[i] - yr_data[i-1]
  end
p dly_data
p dly_data.find_index {|e| e<0}
exit
  data << dly_data
end # year
plot_graph(data)


#--- sources of data
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/zensu01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teiten01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teitenrui01.csv
