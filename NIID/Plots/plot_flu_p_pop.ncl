load "$SysE/lib/ncl/helper_libs.ncl"

function get_flu_data(pref,idx)
local flu, flu0, flu1, fnm
begin
  pp(pref)
  flu = new( (/52*3/),"float")
  flu = 0.0
  ;if idx.ge.1
    if pref.eq."Total.csv"
      return (flu)
    end if
  ;end if
  fnm = "../Influenza/2017/"+pref
  flu0 = asciiread(fnm,(/52,5/),"float") 
  flu(:51) = flu0(:,idx)
  fnm = "../Influenza/2018/"+pref
  flu1 = asciiread(fnm,(/52,5/),"float") 
  flu(52:103) = flu1(:,idx)
  fnm = "../Influenza/2019/"+pref
  flu2 = asciiread(fnm,(/52,5/),"float") 
  flu(104:) = flu2(:,idx)
  return(flu)
end

function get_pop_dens(fnm)
begin
  data=asciiread(fnm,-1,"string")
  nrow=dimsizes(data)
  prefs = new(nrow,"string")
  popd  = new(nrow,"float")
  do i = 0,nrow-1
    tmp = str_split(data(i),",")
    prefs(i) = tmp(1)
    popd(i) = to_float(tmp(2))
  end do
  return([/prefs,popd/])
end

function rep_rate(arr)
begin
  rrate=arr*1.0
  sm_arr = runave(arr,3,0)
  narr=dimsizes(arr)
  do i=1,narr-2
    rrate(i) = 1.0*sm_arr(i)/sm_arr(i-1)
  end do
  rrate(0) = 0.0
  rrate(narr-1) = 0.0
  return(rrate)
end
res = get_pop_dens("../../HealthStats/population_density.csv")
pop_res = get_pop_dens("../../HealthStats/population_prefs.csv")
prefs = res[0]
popd = res[1]
pref2 = pop_res[0]
popln = pop_res[1]
delete(res)
delete(pop_res)
xlabs = prefs
prefs = prefs+".csv"

;prefs = (/"Okinawa","Total"/)+".csv"

idx = 1 ; weekly flu no
idx = 3 ; accumulated flu
nprfs = dimsizes(prefs)
flu = new((/nprfs,52*3/),"float")
rflu = new((/nprfs,52*3/),"float")
mflu = new((/nprfs/),"float")
do i = 0,nprfs-1
  tmp_flu = get_flu_data(prefs(i),idx)
  flu(i,:) = tmp_flu
  ; plot accumalated flu cases at end of year (t=155)
  ;   divided by population (per 100,000 people) for that prefecture
  mflu(i) = flu(i,155)*100000/popln(i)*1.0
end do



xvals = ispan(1,nprfs,1)
ymax = max(flu)
ymax = ymax+0.1*ymax
ymin = 0.0

wks = open_wks("eps","inflz_popln","default")
resL = True
resL@vpWidthF = 0.7
resL@vpHeightF = 0.2
resL@tmXBMode = "Explicit"
resL@tmXBValues = xvals
resL@tmXBLabels = xlabs
resL@tmXBLabelAngleF = 45
resL@tmXBLabelFontHeightF = 0.006
resL@tiYAxisFontHeightF = 0.009

resR                        = resL
resR@xyLineThicknesses      = 2 
resR@xyLineColor      = "black"

resL@tiYAxisString = "Population density (people/square km)"
resR@tiYAxisString = "Yearly cases of Influenza per 100,000 people "
resL@gsnYRefLine = 0
resL@gsnXYBarChart=True
resL@gsnXYBarChartColors="green"

plot = gsn_csm_xy2(wks,xvals,popd,mflu,resL,resR)
;plot = gsn_csm_xy2(wks,time,flu(2,:), flu(0:1,:),res,resR)
