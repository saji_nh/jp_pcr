require "#{ENV['HOME']}/Software/GR/GRruby-extension/lib/grruby"

def plot_graph(yarr)
  g = Rubyplot::Figure.new
  y = yarr.first

  (1..yarr.length).each do |i|
    @y1 = yarr.shift
    @x1 = [*(1..@y1.length)]
    g.line! @x1, @y1, line_width: 4, line_color: :green
  end
  g.view
end

