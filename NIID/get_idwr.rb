require 'fileutils'
require 'date'
require './idwr'

reports = ["zensu", "teiten", "teitenrui"]
url     = "https://www.niid.go.jp/niid/images/idwr/data-e"

#year = 2020
(2016..2020).to_a.each do |year|
 p year
#(2015..2016).to_a.each do |year|
  nweeks = num_weeks(year) #.to_i
  nweeks = 39 if year==2020
  (1..nweeks).to_a.each do |iwk|
    fmt_wk = "%02d" % iwk
    dir_path = "idwr-e#{year}/#{year}#{fmt_wk}"
    if year==2013
      dir_path = "#{(year-2000)}#{fmt_wk}"
    end
    reports.each do |report|
      rep_dir = File.join(report,year.to_s)
      FileUtils.mkdir_p rep_dir
      Dir.chdir(rep_dir) do
        rep_path = "#{report}#{fmt_wk}.csv"
        fil_path = File.join(url,dir_path,rep_path)
        next if File.exist? rep_path
        p "retrieving #{fil_path}"
        `wget #{fil_path}`
      end
    end # reports
  end # week
end # year


#--- sources of data
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/zensu01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teiten01.csv
#https://www.niid.go.jp/niid/images/idwr/data-e/idwr-e2020/202001/teitenrui01.csv
