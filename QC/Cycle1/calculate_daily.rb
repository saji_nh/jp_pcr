require 'pp'
require 'fileutils'


prefs = IO.readlines("../../prefectures.list")
in_dir = "./Total"
out_dir = "./Daily"
FileUtils.mkdir_p(out_dir)
prefs.each do |pref|
  puts pref
  file_path = "#{File.join(in_dir,pref.chomp)}.csv"
  org_data = IO.readlines("../../Total/Tokyo.csv")[1..-1]
  data = IO.readlines(file_path) # start from 1 to ignore the header
  out_path = "#{File.join(out_dir,pref.chomp)}.csv"
  fout = File.open(out_path,"w")
  # column headers are listed below
  # year,month,day,pcr_positive,pcr_tests,discharged,deaths
  fout.puts "year,month,day,pcr_positive,pcr_tests,discharged,deaths"
  positiv, tests, disch,death = 0,0,0,0
  org_data.each_with_index do |d,i| 
    
    np,nt = data[i].split(",").map {|s| Integer(s)}
    if np == -999
       np = 0
       nt = 0
    end
    str_arr=d.chomp.split(",")
    y,m,d=str_arr[0..2]
    p,t,di,de=str_arr[3..-1].map {|s| Integer(s)}
    new_p, new_t, new_di, new_de = np,nt,di,de
    np  = np-positiv
    nt  = nt-tests
    di = di-disch
    de = de-death
    positiv, tests,disch,death = new_p,new_t,new_di,new_de
    if np < 0
      np = 0
      nt = 0
    end
    fout.puts [y,m,d,np,nt,di,de].join(",")
  end
  fout.close
end
