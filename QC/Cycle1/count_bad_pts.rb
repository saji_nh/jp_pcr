require 'fileutils'

# Write bad data to odir1
odir1 = "../../BadDaily"
odir2 = "../../GooDaily"
FileUtils.mkdir_p odir1
FileUtils.mkdir_p odir2

prefs = IO.readlines("../../prefectures.list")
outf  = "bad_points.csv"
counts =[]
prefs.each do |pref|
  pref.chomp!
  file_path = "../../Daily/#{pref}.csv"
  tmp = IO.readlines(file_path)
  hdr = tmp[0]
  data = tmp[1..-1]
  bad_pos = 0
  bad_tst = 0
  nbad = 0
  bad_dly = []
  goo_dly = []
  data.each do |d| 
    y,m,dy,p,t,di,de,ct=d.chomp.split(",")
    if (Integer(p) !=0) && (Integer(t) <= Integer(p))
      bad_pos += Integer(p)
      bad_tst += Integer(t)
      nbad += 1
      bad_dly << [y,m,dy,Integer(p),Integer(t),di,de,ct].join(",")
      goo_dly << [y,m,dy,-999,-999,di,de,ct].join(",")
    else
      bad_dly << [y,m,dy,-999,-999,di,de,ct].join(",")
      goo_dly << [y,m,dy,Integer(p),Integer(t),di,de,ct].join(",")
    end # endif
  end # loop thru data
  counts << [pref, bad_pos, bad_tst, nbad,data.length] #.join(",")
  File.open(File.join(odir1,"#{pref}.csv"),"w") do |of|
    of.puts hdr
    of.puts bad_dly
  end
  File.open(File.join(odir2,"#{pref}.csv"),"w") do |of|
    of.puts hdr
    of.puts goo_dly
  end
end # loop for prefectures
scounts = counts.sort_by {|c| c[3]}
scounts.reverse!
ncounts = []
scounts.each do |s|
  b = s[3]
  t = s[4]
  ncounts << ["[#{s[0]}](/images/PosPCR/#{s[0]}.png)", s[1], s[2], (b==0 ? 0.0 : (100.0*b/t).round(1))] #.join(",")
end


File.open(outf,"w") do |of| 
  of.puts "| Prefecture | No of PCR positives | No of PCR tests | Percentage of bad points| "
  of.puts "|----------|:-------------:|:------:|:---------:|"
  of.puts ncounts.map {|s| "|"+s.join(" | ")+"|"}
end
#[Osaka](/images/PosPCR/Osaka.png)

