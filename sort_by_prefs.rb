require 'pp'
require 'csv'
require 'matrix' # need to store prev. date for each prefecture
# keep this at the top for now
#  till I make a class. Or, should I?

BASE_TIME = Time.new(2020,1,1).to_i

def to_time(arr)
  y,m,d=arr[0..2]
  (Time.new(y,m,d).to_i - BASE_TIME)/(86400)
end

def split_string(str)
  #str.split(",").map {|e| e.delete('"')}
  ans = CSV.parse(str).flatten
    # in data on or after Aug 28, cols 5..10 are
    #     5,       6,         7,          8,        9,      10
    #  positives,tests,hospitalizations,serious,discharges,deaths
    # in data before Aug 28, cols 5..10 are
    #  positives,tests,discharges,deaths
    # we delete at 7,8 to remove hospitalizations and serious cases
    # or successively at position 7 
  abort "array size wrong" if ans.length !=12
  # we first remove the last element
  ans.pop
  ans.delete_at(7)
  ans.delete_at(7)
  return ans
end

def parse_string(str,prefs) #,date_arr,start_flag)
  arr = split_string(str)
  pnm = arr[4].chomp 
  abort("Unexpected prefecture name #{pnm}") unless prefs.any? {|prf| prf==pnm}
  iprf = prefs.find_index(pnm)
  ctim = to_time(arr)
  begin
    return_str = arr[5..10].map do |e| 
                   e.gsub!("-","0") if e
                   e.nil? ? e=0 : Integer(e)
                 end
    return_str << ctim
  rescue Exception => e 
    p "positives,tests,hospitalizations,serious,discharges,deaths"
    p arr
    p e.message
    exit
  end
  return([iprf,ctim,return_str.join(",")])
end


fnm = "covid19/data/prefectures.csv" # source data
prefs = IO.readlines("prefectures.list").map {|p| p.chomp}
nprfs = prefs.size

str_arr = IO.readlines(fnm)
str_arr.shift  # this will push the header out of the data
sdy = to_time(split_string(str_arr[0].chomp))
edy = to_time(split_string(str_arr[-1].chomp))
ndy = edy-sdy+1

all_dates=[]
cod_dts=[]
fdy = sdy
(sdy..edy).to_a.each do |dy|
  all_dates << Time.at(dy*86400+BASE_TIME).strftime("%Y,%-m,%-d")
  cod_dts << fdy
  fdy+=1
end

# prefill with all available dates
prf_data = Matrix.build(nprfs,ndy) {|r,c| [["2020,1,1"],["a,b,c"]]}
(0..ndy-1).to_a.each do |idy|
  (0..nprfs-1).to_a.each do |ipr|
    prf_data[ipr,idy][0] = all_dates[idy]
    prf_data[ipr,idy][1] = ["0,0,0,0,#{cod_dts[idy]}"]
  end
end

# open files to write out data per prefecture
out_dir = "./Total"
outfil_handles=[]
  prefs.each do |pref|
    outfil_handles << File.open("#{File.join(out_dir,pref.chomp)}.csv","w")
  end

outfil_handles.each do |of|
  of.puts "year,month,day,pcr_positive,pcr_tests,discharged,deaths,coded_time"
end

# Exit with warning, if we see any problem with start or end time stamps
abort("Problems with time stamp in #{fnm}") if (sdy<=0 || edy<=0)

  # in recent files (prefectures.csv), data from Tokyo alone is
  # added from Feb 8 to Mar 10; other prefectures have not yet
  # reported data for these dates.
  #   to handle this we store date of current entry and
  #   check it with following entry, and make sure we are
  #   retrieving and writing dates consecutively without
  #   duplicates

str_arr.each_with_index do |str,i|

  str.chomp!
  iprf,ctim,data = parse_string(str,prefs) #,prf_date,i)
  idy = ctim-sdy
  prf_data[iprf,idy][1] = data
end

outfil_handles.each_with_index do |of,iprf|
  (0..ndy-1).to_a.each do |idy|
    of.puts (prf_data[iprf,idy].flatten).join(",")
  end
end

p "Exiting loop"
outfil_handles.each {|of| of.close}


