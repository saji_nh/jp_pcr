require 'pp'
require 'fileutils'
require '../som_input'

include SOM_Input

out_dir = "./"

prefs = IO.readlines("../../prefectures.list")
in_dir = "../../Weekly"
FileUtils.mkdir_p(out_dir)
out_path = "#{File.join(out_dir,'prvl_comp')}.txt"
fout = File.open(out_path,"w")
prefs.each_with_index do |pref,indx|
  puts pref

  file_path = "#{File.join(in_dir,pref.chomp)}.csv"
  all_data = IO.readlines(file_path) # start from 1 to ignore the header
  data = all_data[1..-1] # start from 1 to ignore the header


  # choose one data per week
  new_data = data.values_at(*(0..data.length-1).step(7))
  # typ =>[rep_rate,tests]
    # arr[7] holds coded time; <=153 means <= June 2
  diag_tests = Integer((split_and_retrieve(new_data,:typ=>"num_tests",:start_from=>(38),
                                                  :end_at=>(153-7))).sum)
  diag_pstvs = Integer((split_and_retrieve(new_data,:typ=>"num_positives",:start_from=>(38),
                                                  :end_at=>(153-7))).sum)

  scrn_tests = Integer((split_and_retrieve(new_data,:typ=>"num_tests",
                               :start_from=>(153))).sum)
  scrn_pstvs = Integer((split_and_retrieve(new_data,:typ=>"num_positives",
                                   :start_from=>(153))).sum)

  diag_prvl = (diag_tests>0? (100.0*diag_pstvs/diag_tests).round(2) : 0.0)
  scrn_prvl = (scrn_tests>0? (100.0*scrn_pstvs/scrn_tests).round(2) : 0.0)
  fout.puts [pref.chomp,diag_tests,diag_pstvs,diag_prvl,scrn_tests,scrn_pstvs,scrn_prvl].join(",")
end  # loop over prefectures
fout.close
