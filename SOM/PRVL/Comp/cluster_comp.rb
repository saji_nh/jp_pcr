require 'pp'
require 'matrix'
require 'fileutils'
require '../../som_input.rb'
include SOM_Input

def process_prefs(in_dir,pref_list)
  tests = []
  pstvs = []
  pref_list.each do |pref|  
    file_path = "#{File.join(in_dir,pref.chomp)}.csv"
    all_data = IO.readlines(file_path) # start from 1 to ignore the header
    data = all_data[1..-1] # start from 1 to ignore the header


    # choose one data per week
    new_data = data.values_at(*(0..data.length-1).step(7))
    # typ =>[rep_rate,tests]
    tests<<  split_and_retrieve(new_data,:typ=>"num_tests")
    pstvs<<  split_and_retrieve(new_data,:typ=>"num_positives")
  end
  if tests.length > 1
    tsum = (tests.map { |a| Vector.elements(a) }.inject(:+)).to_a
    psum = (pstvs.map { |a| Vector.elements(a) }.inject(:+)).to_a
  else
    tsum = tests[0]
    psum = pstvs[0]
  end
  prvl = []
  psum.each_index do |i|
    prvl << (tsum[i] <= 0 ? 0 : (100.0*psum[i]/tsum[i]).round(2))
  end
  prvl
end

def write_output(fnm,data)
  fout = File.open(fnm,"w")
  fout.puts data
  fout.close 
end

required_metric = "prevalence_rate"
fin = File.open("../som_inp.fin_map","r")
ncol,nrow = fin.gets.split[2..3].map {|e| Integer(e)}
mdir = "../node_members"
in_dir = "../../../Weekly"
ot_dir = "./Out"
FileUtils.mkdir_p ot_dir
members = []
nrow.times.each do |y|
  ncol.times.each do |x|
    mfil = File.join(mdir,"som#{x}_#{y}")
    if File.exists? mfil
      write_output("#{ot_dir}/cluster_#{x}#{y}", process_prefs(in_dir,IO.readlines(mfil)))
    end
  end
end
