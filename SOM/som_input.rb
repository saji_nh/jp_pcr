module SOM_Input

  def normalized_value(val)
    amx = val.max
    val.map! {|e| e/= amx ; e.round(2)}
    return(val)
  end

  def prevalence_rate(pos,tsts)
    prvl = []
    tsts.each_index do |i|
      prvl << (tsts[i] <= 0 ? 0 : (100.0*pos[i]/tsts[i]).round(2))
    end
    return(prvl)
  end

  def reproduction_rate(varr)
    vlen=varr.length
    narr = []
    narr << 0
    (1..vlen-1).to_a.each do |i|
      if varr[i-1] > 0.0
        narr << (varr[i]/varr[i-1]).round(2)
      else
        narr << 0.0
      end
    end
    return(narr[1..-1])
  end

# Here we split the data into two parts, one before current testing
# regime
  def split_and_retrieve(str_arr,hsh)
    # arr[7] holds coded time; <=153 means <= June 2
    p,t=[],[]
    str_arr.each do |str|
      arr = str.chomp.split(",")
      if hsh[:start_from]
        next if (Integer(arr[7]) <= hsh[:start_from])
      end
      if hsh[:end_at]
        next if (Integer(arr[7]) >= hsh[:end_at])
      end
      #next if (Integer(arr[7]) <= (153-7))
      p << Float(arr[3])*1.0
      t << Float(arr[4])*1.0
    end
    case hsh[:typ]
      when "normalized_tests"
        ans = normalized_value(t)
      when "growth_rate_tests"
        ans = reproduction_rate(t)
      when "prevalence_rate"
        ans = prevalence_rate(p,t)
      when "num_tests"
        ans = t
      when "num_positives"
        ans = p
      else
        abort "Incorrect specs, use: normalized_tests, growth_rate_tests, or prevalence_rate"
    end
    return(ans)
  end 

end # end of Module definition
