require 'pp'
require 'fileutils'
require './som_input'

include SOM_Input

INP_TYP = "prevalence_rate"

out_dir = "TSTN"
out_dir = "PRVL" if INP_TYP == "prevalence_rate"

prefs = IO.readlines("../prefectures.list")
in_dir = "../Weekly"
FileUtils.mkdir_p(out_dir)
out_path = "#{File.join(out_dir,'som_inp')}.txt"
fout = File.open(out_path,"w")
dlen = []
prefs.each_with_index do |pref,indx|
  puts pref

  file_path = "#{File.join(in_dir,pref.chomp)}.csv"
  all_data = IO.readlines(file_path) # start from 1 to ignore the header
  data = all_data[1..-1] # start from 1 to ignore the header
  #hdr  = all_data[0].chomp


  # choose one data per week
  new_data = data.values_at(*(0..data.length-1).step(7))
  # typ =>[rep_rate,tests]
    # arr[7] holds coded time; <=153 means <= June 2
  #res = split_and_retrieve(new_data,:typ=>INP_TYP,:start_from=>(153-7))
  res = split_and_retrieve(new_data,:typ=>INP_TYP,:start_from=>(38))
  if indx==0
    fout.puts "#{res.length} rect 8 5 gaussian"
  end
  fout.puts "#{res.join(" ").strip} #{pref}"
  dlen << res.length

end  # loop over prefectures
fout.close
#puts dlen
