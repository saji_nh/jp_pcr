require 'pp'
require 'fileutils'

def split_and_retrieve(str_arr)
  prvl = []
  str_arr.each do |str|
    p,t = str.chomp.split(",")[3..4]
    prvl << (Integer(t)==0 ? 0 : Integer(100.0*Float(p)/Float(t)))
  end
  return(prvl[6..-1])
end

prefs = IO.readlines("../prefectures.list")
in_dir = "../Weekly"
ot_dir = "./"
FileUtils.mkdir_p(ot_dir)
out_path = "#{File.join(ot_dir,'som_inp')}.txt"
fout = File.open(out_path,"w")
dlen = []
fout.puts "21 rect 8 5 gaussian"
prefs.each do |pref|
  puts pref

  file_path = "#{File.join(in_dir,pref.chomp)}.csv"
  all_data = IO.readlines(file_path) # start from 1 to ignore the header
  data = all_data[1..-1] # start from 1 to ignore the header
  #hdr  = all_data[0].chomp


  # choose one data per week
  new_data = data.values_at(*(0..data.length-1).step(7))
  res = split_and_retrieve(new_data)
  fout.puts "#{res.join(" ")} #{pref}"
  dlen << res.length

end  # loop over prefectures
fout.close
puts dlen
