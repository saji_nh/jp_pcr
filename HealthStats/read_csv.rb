require 'csv'
require 'fileutils'

def process_array(arr)
  yr = Integer(arr[0])
  dt = Integer(arr[3])
  [yr,dt]
end
 

fnm = "population"
odir = "Population"
FileUtils.mkdir_p odir

data = []
fin = File.open("#{fnm}.csv","r")
  fin.gets
while (str=fin.gets) do
  data <<  process_array(CSV.parse(str).first)
end
CSV.open(File.join(odir,fnm+".csv"),"w") do |csv|
    data.each do |d|
      csv << d
    end
end
