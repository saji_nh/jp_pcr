require 'csv'
require 'fileutils'

def process_array(arr)
 # remove first two elements from arr
  2.times do
   arr.shift
  end
  arr.map! do |s|
   s=s.strip.gsub(/\s*/,"")
   /\d+(\.[\d]+)*/.match(s) ? Float(s) : 0.0
  end
  arr
end
 

odir = "Conditions"
FileUtils.mkdir_p odir

#"Showa 35 （1960）","Showa 40 （1965）","Showa 45 （1970）","Showa 50 （1975）","Showa 55 （1980）","Showa 60 （1985）","Heisei 2 （1990）","Heisei 7 （1995）","Heisei 12 （2000）","Heisei 17 （2005）","Heisei 22 （2010）","Heisei 23 （2011）","Heisei 24 （2012）","Heisei 25 （2013）","Heisei 26 （2014）","Heisei 27 （2015）","Heisei 28 （2016）","Heisei 29 （2017）","Heisei 30 （2018）"

yrs = [1960,1965,1970,1975,1980,1985,1990,1995,2000,2005,2010,2011,2012,2013,
2014,2015,2016,2017,2018]

labs = [:total,:tuberculosis,:cancer,:diabetes,:hypertension, :heart_disease,
  :cerebrovascular, :pneumonia, :chronic_bronchitis, :asthma, :ulcer, :liver,
  :renal, :senility, :accidents, :traffic_accidents, :suicide]
fin = File.open("1-29.csv","r")
labs.each do |lab|
  p lab
  fin.gets
  male = process_array(CSV.parse(fin.gets).first)
  female = process_array(CSV.parse(fin.gets).first)
  CSV.open(File.join(odir,lab.to_s+".csv"),"w") do |csv|
    male.each_index do |i|
      csv << [yrs[i], male[i], female[i]]
    end
  end
end
