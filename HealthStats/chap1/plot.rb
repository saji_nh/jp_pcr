require "#{ENV['HOME']}/Software/GR/GRruby-extension/lib/grruby"

  g = Rubyplot::Figure.new
  y = [1,10,5,3,20]
  x = [1,4,6,7,8]

  g.line! x, y, line_width: 4, line_color: :green
  x = [1,2,6,7,8]
  g.line! x, y, line_width: 4, line_color: :green
  g.view

