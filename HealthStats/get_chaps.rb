require 'fileutils'
url = "https://www.mhlw.go.jp/english/database/db-hh/xlsx"

chap1 = (1..45)
chap2 = (1..68)

FileUtils.mkdir_p("chap2")
Dir.chdir("chap2") do
  chap2.to_a.each do |chp|
    sec =  "%02d" % String(chp) # % "%02d"
    fnm = "2-#{sec}.xlsx"
    next if File.exist? fnm
   p `wget #{url}/#{fnm}`
  end
end

FileUtils.mkdir_p("chap1")
Dir.chdir("chap1") do
  chap1.to_a.each do |chp|
    sec =  "%02d" % String(chp) # % "%02d"
    fnm = "1-#{sec}.xlsx"
    next if File.exist? fnm
   p `wget #{url}/#{fnm}`
  end
end
