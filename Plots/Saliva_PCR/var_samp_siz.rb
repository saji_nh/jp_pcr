require 'gruff'
require 'pp'

#data = [2,-1,4,-1,4,9,-6,-3,-3,5,-2,-5,0,0]
data = [2,1,5,4,8,17,11,8,5,10,8,3,3,3]
dsize = data.size
keys = (0..dsize-1).to_a
dats = (1..dsize).to_a

sel_dats = dats #.select.with_index { |_,i| i%1==0}
sel_keys  = keys #.select.with_index { |_,i| i%1==0}
labels = Hash[sel_keys.zip(sel_dats)]
g = Gruff::Bar.new
g.title = 'Evolution of PCR positive fraction (Tokyo)'
g.labels = labels
#g.theme = Gruff::Themes::ODEO
g.theme = {
         colors: %w(blue purple green white red),
         marker_color: 'blue',
         background_colors: ['white', 'white'],
         background_direction: :top_bottom
       }
              
g.data 'percentage of positive tests', data
g.write('saliva_tst1.gif')
