require 'fileutils'
FileUtils.mkdir_p "./PosPCR"
prefs = IO.readlines("../../prefectures.list")
ncl_script = File.basename($0,".rb")+".ncl"
prefs.each do |pref|
  ENV["JP_PREF_NAME"] =  pref.chomp
  res = `ncl #{ncl_script}`
  puts res
  abort("problem with #{pref}")  if res.match("fatal")
end
